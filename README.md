A simple container intended to allow users to quickly and easily transition from sqlite3 databases to MySQL. By running the container you are in effect running a script.

Prepare a folder holding a `sqlite.db` file and bind-mount it into the container as `/data`. By default, the container will produce a `mysql.sql` file in the same folder.

The Python script that does most of the magic originated from:

  * http://stackoverflow.com/questions/18671/quick-easy-way-to-migrate-sqlite3-to-mysql

The reason we internally copy the source database to `/tmp` is because without this the sqlite3 cli reports an I/O error (code 10). Copying the file resolves this.

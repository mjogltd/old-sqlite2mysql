FROM python:3.5

RUN apt-get update \
  && apt-get install -y sqlite

ADD ./sqlite-to-mysql.py /usr/src/app/
VOLUME /data
CMD cp /data/sqlite.db /tmp/ && echo '.dump' | sqlite3 /tmp/sqlite.db  | python /usr/src/app/sqlite-to-mysql.py  > /data/mysql.sql
